package semestr2;

public class UpTriangleMatrix extends Matrix {
    UpTriangleMatrix(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Размер матрицы должен быть больше 0");
        } else {
            this.size = size;
            this.matrix = new double[(size * size + size) / 2];
        }
    }
    UpTriangleMatrix(int... set) throws IllegalArgumentException {
        if (set.length <= 0) {
            throw new IllegalArgumentException("Задан неккоректный размер матрицы");
        } else {
            this.size = set.length;
            this.matrix = new double[(size * size + size) / 2];
            for (int i = 0; i < matrix.length; i++) {
                this.matrix[i] = set[i];
            }
        }
    }
    @Override
    public double getElemIndex(int index1, int index2) {
        if (index1 >= size || index2 >= size || index1 < 0 || index2 < 0) {
            throw new IllegalArgumentException("Индексы не входят в матрицу");
        } else if (index1 > index2) {
            return 0.0;
        } else {
            return matrix[(size - index1) * index1 + index2];
        }
    }

    @Override
    public void setElemIndex(int index1, int index2, double newElem) {
        if (index1 >= size || index2 >= size || index1 < 0 || index2 < 0) {
            throw new IllegalArgumentException("Индексы не входят в матрицу");
        }
        else if (index1 > index2 && newElem != 0) {
            throw new IllegalArgumentException("Нельзя записать ненулевое значение под главной диагональю");
        } else {
            matrix[(size - index1) * index1 + index2] = newElem;
            flag = false;
        }
    }

    @Override
    public double determinant() {
        double determinant = 1;
        if (flag) {
            return determinantCache;
        }
        for (double i : matrix) {
            determinant *= i;
        }
        determinantCache = determinant;
        flag = true;
        return determinant;
    }
}
